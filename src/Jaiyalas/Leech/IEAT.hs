{-# LANGUAGE OverloadedStrings #-}
-- -- -- -- -- -- -- -- -- -- -- -- -- --
module Jaiyalas.Leech.IEAT where
-- -- -- -- -- -- -- -- -- -- -- -- -- --
import           Control.Monad
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
import           Text.Regex.TDFA
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy as BS
import qualified Data.Text as T
-- ---           ----------------------
import qualified Codec.Text.IConv as ICONV
-- ---           ----------------------
import           Jaiyalas.Leech.JSON
import           Jaiyalas.Leech.URLs
-- -- -- -- -- -- -- -- -- -- -- -- -- --

--

--
{- ===== ===== IEAT ===== ===== -}
--
getMsgIEAT :: IO (Either String INEs)
getMsgIEAT = do
  rs <- httpLBS =<< (parseRequest ieatNewsURL)
  return $ eitherDecode $ (ICONV.convert "BIG5" "UTF-8") $ getResponseBody rs
--
data IEATNewsEntity = IEATNewsEntity
    { ine_id :: Int
    , ine_tl :: T.Text
    , ine_pt :: T.Text
    } deriving (Eq)
--
instance FromJSON IEATNewsEntity where
    parseJSON = withObject "IEATNewsEntity" $ \o -> do
                  --
                  _id <- o .: "id"
                  _tl <- o .: "tl"
                  _pt <- o .: "pt"
                  return $ IEATNewsEntity _id _tl _pt
                  --
--
instance Show IEATNewsEntity where
  show x =
    "["++(T.unpack $ ine_pt x)++"] " ++
    "http://www.ieatpe.org.tw/nboard/view.aspx?id="++(show $ ine_id x)++" \n" ++
    "    "++(T.unpack $ ine_tl x)++" \n" ++
    ""
--
data INEs = INEs {ines :: [IEATNewsEntity]} deriving (Show, Eq)
--
instance FromJSON INEs where
  parseJSON v = do
    is <- pp v
    return $ INEs is
--
pp :: Value -> Parser [IEATNewsEntity]
pp = intoObj "jdata"
   $ intoObj "li" parseJSON 
--
intoObj :: FromJSON json => T.Text -> (json -> Parser a) -> Value -> Parser a
intoObj t f = withObject "" (\o -> (o .: t) >>= f) 
--
